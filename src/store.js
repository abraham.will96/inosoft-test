import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store(
  {
    state:{
      uom:[
        'SHP',
        'BAG',
        'BX'
      ],
      currency:[
        'USD',
        'IDR',
        'AED'
      ],
      chargeTo:[
        'SHP01',
        'SHP02',
        'Other'
      ] 
    },

    getters:{

    },
    mutations:{

    },
    actions:{

    }
  }
)