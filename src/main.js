import Vue from 'vue'

import store from "./store";

import axios from 'axios'
import VueAxios from 'vue-axios'

import vSelect from "vue-select"
import "vue-select/dist/vue-select.css";

import "bootstrap"
import "bootstrap/dist/css/bootstrap.min.css"

import '../node_modules/bootstrap-icons/font/bootstrap-icons.css'

import apiFetcher from './global_scripts/apiFetcher.js'


import App from './App.vue'
import "./main.css"

Vue.config.productionTip = false
Vue.use(VueAxios, axios)
Vue.mixin(apiFetcher)
Vue.component("v-select", vSelect);

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
